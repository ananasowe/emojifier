#include <iostream>
#include "emoji.hpp"
using namespace std;
int main(int argc, char *argv[]) {
	if(argc > 1) {
		cout << emojicpp::emojize(argv[1]) << endl;
		return 0;
	} else {
		cerr << "Please enter names of emoji(s) as a program argument" << endl;
		return 1;
	}
}
